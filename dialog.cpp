#include "dialog.h"
#include "ui_dialog.h"
#include <QFileDialog>
#include <QMessageBox>

enum {
    ImagenCoyote,
    ImagenCorrecaminos,
    ImagenCantidad // canti
};

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    imagenes[0] = "Coyote.png";
    imagenes[1] = "Correcaminos.png";
    imagenes[2] = "Bart.png";

    imagen = 0; // La primer imagen del se carga en dialog.ui
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
          tr("Open Image"), "/home/jana", tr("Image Files (*.png *.jpg *.bmp)"));

    QPixmap pix;
    if(!pix.load(fileName)) {
        QMessageBox::warning(this, tr("Imagen"), tr("Error abriendo imagen!"));
        return;
    }
    ui->label->setPixmap(pix);
    imagen = -1;
}

void Dialog::on_pushButton2_clicked()
{
    QPixmap pix;

    imagen = (imagen + 1) % TOTAL_IMAGENES_PRECARGADAS;

    if(!pix.load(":/image/" + imagenes[imagen])) {
        QMessageBox::warning(this, tr("Imagen"), tr("Error abriendo imagen ") + imagenes[imagen] + "!");
        return;
    }
    ui->label->setPixmap(pix);
}
