#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

#define TOTAL_IMAGENES_PRECARGADAS 3

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_pushButton_clicked();

    void on_pushButton2_clicked();

private:
    Ui::Dialog *ui;
    int imagen;
    QString imagenes[TOTAL_IMAGENES_PRECARGADAS];
};

#endif // DIALOG_H
